FROM golang:1.16-alpine

RUN apk add --no-cache git

# Set the Current Working Directory inside the container
WORKDIR /opt/app

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .

RUN go mod download

COPY . .

# Build the Go app
RUN go build -o ./main .


# HEALTHCHECK --interval=1m --timeout=3s --retries=3 \
# CMD curl -f http://localhost:3000/healthcheck || exit 1

# This container exposes port 3030 to the outside world
EXPOSE 3000

# Run the binary program produced by `go install`
CMD ["./main"]
