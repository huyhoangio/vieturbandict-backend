package db

import (
	"context"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var mongoDB *mongo.Database

func MongoDBConnect() {
	if mongoDB == nil {
		mongoDB = mongoDBConnect()
	}
}

func MongoDB() *mongo.Database {
	return mongoDB
}

type MongoDBConfig struct {
	URL      string
	User     string
	DBName   string
	Password string
}

func buildConfig() *MongoDBConfig {
	dbConfig := MongoDBConfig{
		URL:      os.Getenv("MONGODB_URL"),
		User:     os.Getenv("MONGODB_USER"),
		Password: os.Getenv("MONGODB_PASSWORD"),
		DBName:   os.Getenv("MONGODB_DBNAME"),
	}
	return &dbConfig
}

func getURI(conf *MongoDBConfig) string {
	uri := conf.URL
	return uri
}

func mongoDBConnect() *mongo.Database {
	dbConfig := buildConfig()
	uri := getURI(dbConfig)

	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Connected to MongoDB!")

	return client.Database(dbConfig.DBName)
}
