package db

import (
	"log"
	"os"

	"github.com/go-redis/redis/v8"
)

var rdb *redis.Client

func RedisConnect() {
	if rdb == nil {
		rdb = redisConnect()
	}
	log.Println("Redis connected!")
}

func redisConnect() *redis.Client {
	dsn := os.Getenv("REDIS_DSN")
	if len(dsn) == 0 {
		dsn = "localhost:6379"
	}
	client := redis.NewClient(&redis.Options{
		Addr: dsn,
	})
	return client
}

func Redis() *redis.Client {
	return rdb
}
