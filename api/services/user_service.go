package services

import (
	"vieturbandict/api/models"
	"vieturbandict/api/repos"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserService interface {
	FindByID(primitive.ObjectID) (*models.User, error)
	AddNew(*models.User) (primitive.ObjectID, error)
	FindByUserName(string) (*models.User, error)
}

type userService struct {
}

func User() UserService {
	return &userService{}
}

func (*userService) FindByID(_id primitive.ObjectID) (*models.User, error) {
	user, err := repos.User().FindByID(_id)

	return user, err
}

func (*userService) AddNew(user *models.User) (primitive.ObjectID, error) {
	userID, err := repos.User().InsertOne(user)

	return userID, err
}

func (*userService) FindByUserName(username string) (*models.User, error) {
	filter := bson.M{"username": username}
	user, err := repos.User().FindOne(filter)

	return user, err
}
