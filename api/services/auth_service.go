package services

import (
	"context"
	"net/http"
	"os"
	"strings"
	"time"
	"vieturbandict/api/common"
	"vieturbandict/db"
	"vieturbandict/rest"

	"github.com/dgrijalva/jwt-go"
	"github.com/twinj/uuid"
)

type AuthService interface {
	GenerateAccessToken(string) (string, error)
	GenerateRefreshToken(string, string) (string, error)
	ExtractToken(*http.Request) string
	ParseToken(string, string) (*common.JWTClaims, error)
	GetIP(r *http.Request) string
}

type authService struct {
}

func Auth() AuthService {
	return &authService{}
}

func (*authService) GenerateAccessToken(userID string) (string, error) {
	var err error
	token := ""
	secretKey := os.Getenv("JWT_ACCESS_SECRET")
	expDuration := time.Minute * 15
	exp := time.Now().Add(expDuration).Unix()
	uuid := uuid.NewV4().String()
	atClaims := jwt.MapClaims{}
	atClaims["user_id"] = userID
	atClaims["exp"] = exp
	atClaims["rid"] = uuid
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err = at.SignedString([]byte(secretKey))

	if err == nil {
		err = db.Redis().Set(context.TODO(), uuid, userID, expDuration).Err()
	}

	return token, err
}

func (*authService) GenerateRefreshToken(userID, ip string) (string, error) {
	var err error
	token := ""
	secretKey := os.Getenv("JWT_REFRESH_SECRET")
	expDuration := time.Hour * 7 * 24
	exp := time.Now().Add(expDuration).Unix()
	uuid := uuid.NewV4().String()
	rfClaims := jwt.MapClaims{}
	rfClaims["user_id"] = userID
	rfClaims["ip"] = ip
	rfClaims["rid"] = uuid
	rfClaims["exp"] = exp
	rf := jwt.NewWithClaims(jwt.SigningMethodHS256, rfClaims)
	token, err = rf.SignedString([]byte(secretKey))

	if err == nil {
		err = db.Redis().Set(context.TODO(), uuid, userID, expDuration).Err()
	}

	return token, err
}

func (*authService) ExtractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func (*authService) ParseToken(accessToken string, secretKey string) (*common.JWTClaims, error) {
	claims := &common.JWTClaims{}

	token, err := jwt.ParseWithClaims(
		accessToken,
		claims,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secretKey), nil
		},
	)
	if err != nil || !token.Valid || claims.ExpiresAt < time.Now().Unix() {
		panic(rest.Error{
			Code:    http.StatusBadRequest,
			Message: "TokenExpired",
		})
	}

	return claims, nil
}

func (*authService) GetIP(r *http.Request) string {
	forwarded := r.Header.Get("X-FORWARDED-FOR")
	if forwarded != "" {
		return forwarded
	}
	return r.RemoteAddr
}
