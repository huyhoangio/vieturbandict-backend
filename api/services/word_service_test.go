package services

import (
	"errors"
	"testing"
	"vieturbandict/api/models"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type mockWordRepo struct {
	mock.Mock
}

func (mockRepo *mockWordRepo) FindAll() ([]models.Word, error) {
	args := mockRepo.Called()

	words := args.Get(0)
	err := args.Error(1)

	return words.([]models.Word), err
}

func (mockRepo *mockWordRepo) FindByID(_id primitive.ObjectID) (*models.Word, error) {
	args := mockRepo.Called()

	word := args.Get(0)
	err := args.Error(1)

	return word.(*models.Word), err
}

func (mockRepo *mockWordRepo) InsertOne(word *models.Word) (primitive.ObjectID, error) {
	args := mockRepo.Called()

	_id := args.Get(0)
	err := args.Error(1)

	return _id.(primitive.ObjectID), err
}

func (mockRepo *mockWordRepo) DeleteOne(_id primitive.ObjectID) error {
	args := mockRepo.Called()

	err := args.Error(0)

	return err
}

func TestGetRecentsSuccess(t *testing.T) {
	mockRepo := new(mockWordRepo)

	_id := primitive.NewObjectID()
	word := models.Word{
		ID:                             _id,
		Word:                           "word",
		Explanation_in_Vietnamese:      "explanation_in_vietnamese",
		Explanation_in_English:         "explanation_in_englist",
		Literal_translation_to_English: "literal_translation_to_english",
		Equivalence_in_English:         []string{"equivalence_in_english"},
		Upvote:                         0,
		Downvote:                       0,
		Status:                         "available",
	}
	mockRepo.On("FindAll").Return([]models.Word{word}, nil)

	result, err := mockRepo.FindAll()

	mockRepo.AssertExpectations(t)
	assert.Equal(t, _id, result[0].ID)
	assert.Equal(t, "available", result[0].Status)
	assert.Nil(t, err)
}

func TestFindByIDSuccess(t *testing.T) {
	mockRepo := new(mockWordRepo)

	_id := primitive.NewObjectID()
	word := models.Word{
		ID:                             _id,
		Word:                           "word",
		Explanation_in_Vietnamese:      "explanation_in_vietnamese",
		Explanation_in_English:         "explanation_in_englist",
		Literal_translation_to_English: "literal_translation_to_english",
		Equivalence_in_English:         []string{"equivalence_in_english"},
		Upvote:                         0,
		Downvote:                       0,
		Status:                         "available",
	}
	mockRepo.On("FindByID").Return(&word, nil)

	result, err := mockRepo.FindByID(_id)

	mockRepo.AssertExpectations(t)
	assert.Equal(t, _id, result.ID)
	assert.Equal(t, "word", result.Word)
	assert.Equal(t, "available", result.Status)
	assert.Nil(t, err)
}

func TestAddNewSuccess(t *testing.T) {
	mockRepo := new(mockWordRepo)

	_id := primitive.NewObjectID()
	word := models.Word{
		ID:                             _id,
		Word:                           "word",
		Explanation_in_Vietnamese:      "explanation_in_vietnamese",
		Explanation_in_English:         "explanation_in_englist",
		Literal_translation_to_English: "literal_translation_to_english",
		Equivalence_in_English:         []string{"equivalence_in_english"},
		Upvote:                         0,
		Downvote:                       0,
		Status:                         "available",
	}
	mockRepo.On("InsertOne").Return(word.ID, nil)

	result, err := mockRepo.InsertOne(&word)

	mockRepo.AssertExpectations(t)
	assert.Equal(t, _id, result)
	assert.Nil(t, err)
}

func TestDeleteOneSuccess(t *testing.T) {
	mockRepo := new(mockWordRepo)

	_id := primitive.NewObjectID()
	mockRepo.On("DeleteOne").Return(nil)

	err := mockRepo.DeleteOne(_id)

	mockRepo.AssertExpectations(t)
	assert.Nil(t, err)
}

func TestDeleteOneFailWithNotFound(t *testing.T) {
	mockRepo := new(mockWordRepo)

	_id := primitive.NewObjectID()
	mockRepo.On("DeleteOne").Return(errors.New("can NOT found id"))

	err := mockRepo.DeleteOne(_id)

	mockRepo.AssertExpectations(t)
	assert.EqualError(t, err, "can NOT found id")
}
