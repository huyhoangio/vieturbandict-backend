package services

import (
	"vieturbandict/api/models"
	"vieturbandict/api/repos"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type WordService interface {
	GetRecents(page, size int64, role string, status []string, userId primitive.ObjectID) ([]models.Word, error)
	CountDocuments(page, size int64, role string, status []string, userId primitive.ObjectID) (int64, error)
	AddNew(*models.Word, primitive.ObjectID) (primitive.ObjectID, error)
	Consider(primitive.ObjectID, string) (*models.Word, error)
	DeleteOne(primitive.ObjectID) error
	FindByID(primitive.ObjectID) (*models.Word, error)
}

type wordServiceImpl struct {
}

func Word() WordService {
	return &wordServiceImpl{}
}

func (ws *wordServiceImpl) GetRecents(page, size int64, role string, status []string, userId primitive.ObjectID) ([]models.Word, error) {
	words, err := repos.Word().FindAll(page, size, role, status, userId)

	return words, err
}

func (ws *wordServiceImpl) CountDocuments(page, size int64, role string, status []string, userId primitive.ObjectID) (count int64, err error) {
	count, err = repos.Word().CountDocuments(page, size, role, status, userId)

	return count, err
}

func (*wordServiceImpl) AddNew(word *models.Word, createdBy primitive.ObjectID) (primitive.ObjectID, error) {
	wordID, err := repos.Word().InsertOne(word, createdBy)

	return wordID, err
}

func (ws *wordServiceImpl) Consider(_id primitive.ObjectID, status string) (*models.Word, error) {
	updates := bson.M{"$set": bson.M{"status": status}}
	_, err := repos.Word().UpdateOne(_id, updates)
	if err != nil {
		panic(err)
	}
	word, _ := repos.Word().FindByID(_id)

	return word, nil
}

func (ws *wordServiceImpl) DeleteOne(_id primitive.ObjectID) error {
	err := repos.Word().DeleteOne(_id)

	return err
}

func (*wordServiceImpl) FindByID(_id primitive.ObjectID) (*models.Word, error) {
	word, err := repos.Word().FindByID(_id)

	return word, err
}
