package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID        primitive.ObjectID `json:"_id" bson:"_id"`
	FirstName string             `json:"first_name" bson:"firstName"`
	LastName  string             `json:"last_name" bson:"lastName"`
	UserName  string             `json:"username" bson:"username"`
	Pwd       string             `json:"-" bson:"password"`
	Role      string             `json:"role" bson:"role"`
	CreatedAt time.Time          `json:"created_at" bson:"createdAt"`
	UpdatedAt time.Time          `json:"updated_at" bson:"updatedAt"`
}

func (*User) CollectionName() string {
	return "users"
}
