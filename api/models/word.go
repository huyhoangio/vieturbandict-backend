package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Word struct {
	ID                             primitive.ObjectID `json:"_id" bson:"_id"`
	Word                           string             `json:"word" bson:"word"`
	Explanation_in_Vietnamese      string             `json:"explanation_in_vietnamese" bson:"explanation_in_vietnamese"`
	Literal_translation_to_English string             `json:"literal_translation_to_english" bson:"literal_translation_to_english"`
	Explanation_in_English         string             `json:"explanation_in_english" bson:"explanation_in_english"`
	Equivalence_in_English         []string           `json:"equivalence_in_english" bson:"equivalence_in_english"`
	Upvote                         int                `json:"upvote" bson:"upvote"`
	Downvote                       int                `json:"downvote" bson:"downvote"`
	Status                         string             `json:"status" bson:"status"`
	CreatedBy                      primitive.ObjectID `json:"create_by" bson:"createdBy"`
	UpdatedBy                      primitive.ObjectID `json:"updated_by" bson:"updatedBy"`
	Timestamp                      `bson:",inline"`
}

func (*Word) CollectionName() string {
	return "prod"
}
