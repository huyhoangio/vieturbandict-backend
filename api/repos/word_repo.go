package repos

import (
	"context"
	"log"
	"net/http"
	"time"
	"vieturbandict/api/constants"
	"vieturbandict/api/models"
	"vieturbandict/db"
	"vieturbandict/rest"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type WordRepo interface {
	FindAll(page, size int64, role string, status []string, userId primitive.ObjectID) ([]models.Word, error)
	CountDocuments(page, size int64, role string, status []string, userId primitive.ObjectID) (count int64, err error)
	FindByID(primitive.ObjectID) (*models.Word, error)
	DeleteOne(primitive.ObjectID) error
	InsertOne(*models.Word, primitive.ObjectID) (primitive.ObjectID, error)
	UpdateOne(primitive.ObjectID, bson.M) (*models.Word, error)
}

type wordRepoImpl struct {
}

func Word() WordRepo {
	return &wordRepoImpl{}
}

func (*wordRepoImpl) FindAll(page, size int64, role string, status []string, userId primitive.ObjectID) ([]models.Word, error) {
	var ret []models.Word

	opts := options.Find()
	opts.SetSort(bson.M{"createdAt": -1})
	opts.SetLimit(page * size)
	opts.SetSkip((page - 1) * size)
	filter := bson.M{}
	if role == constants.ADMIN || role == constants.MOD {
		if len(status) != 0 {
			filter["status"] = bson.M{"$in": status}
		}
	} else if userId != primitive.NilObjectID {
		filter["createdBy"] = userId
		if len(status) != 0 {
			filter["status"] = bson.M{"$in": status}
		}
	} else {
		filter["status"] = bson.M{"$in": []string{"approved"}}
	}

	cur, err := db.MongoDB().Collection((&models.Word{}).CollectionName()).Find(context.TODO(), filter, opts)
	if err != nil {
		log.Printf("Something wrong when trying to retrieve records %f", err)
		return nil, err
	}

	for cur.Next(context.TODO()) {
		var elem models.Word
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		ret = append(ret, elem)
	}

	return ret, nil
}

func (*wordRepoImpl) CountDocuments(page, size int64, role string, status []string, userId primitive.ObjectID) (count int64, err error) {
	filter := bson.M{}
	if userId != primitive.NilObjectID && role != constants.ADMIN && role != constants.MOD {
		filter["createdBy"] = userId
	}
	if len(status) == 0 {
		if role != constants.ADMIN && role != constants.MOD {
			filter["status"] = bson.M{"$in": []string{"approved"}}
		}
	} else {
		filter["status"] = bson.M{"$in": status}
	}
	count, err = db.MongoDB().Collection((&models.Word{}).CollectionName()).CountDocuments(context.TODO(), filter)

	return
}

func (*wordRepoImpl) FindByID(_id primitive.ObjectID) (*models.Word, error) {
	var ret models.Word

	ctx := context.TODO()
	err := db.MongoDB().Collection(ret.CollectionName()).FindOne(ctx, bson.M{"_id": _id}).Decode(&ret)
	if err == mongo.ErrNilDocument {
		return nil, err
	}
	if err != nil {
		panic(err)
	}

	return &ret, nil
}

func (repo *wordRepoImpl) DeleteOne(_id primitive.ObjectID) error {
	ctx := context.TODO()
	word, _ := repo.FindByID(_id)
	if word == nil {
		panic(rest.Error{
			Code:    http.StatusNotFound,
			Message: "Word NOT found",
		})
	}

	updates := bson.M{"status": "deleted"}
	_, err := db.MongoDB().Collection((&models.Word{}).CollectionName()).UpdateByID(ctx, _id, updates)
	if err != nil {
		panic(err)
	}
	return nil
}

func (*wordRepoImpl) InsertOne(word *models.Word, createdBy primitive.ObjectID) (primitive.ObjectID, error) {
	word.ID = primitive.NewObjectID()
	word.Status = constants.DEFAULT_WORD_STATUS
	word.Upvote = constants.DEFAULT_WORD_UPVOTE
	word.Downvote = constants.DEFAULT_WORD_DOWNVOTE
	word.CreatedAt = time.Now()
	word.UpdatedAt = time.Now()
	word.CreatedBy = createdBy
	res, err := db.MongoDB().Collection(word.CollectionName()).InsertOne(context.TODO(), word)
	if err != nil {
		return primitive.NilObjectID, err
	}

	return res.InsertedID.(primitive.ObjectID), nil
}

func (repo *wordRepoImpl) UpdateOne(_id primitive.ObjectID, updates bson.M) (*models.Word, error) {
	ctx := context.TODO()
	_, err := db.MongoDB().Collection((&models.Word{}).CollectionName()).UpdateByID(ctx, _id, updates)
	if err != nil {
		panic(err)
	}
	word, _ := repo.FindByID(_id)

	return word, nil
}
