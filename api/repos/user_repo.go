package repos

import (
	"context"
	"time"
	"vieturbandict/api/models"
	"vieturbandict/api/utils"
	"vieturbandict/db"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserRepo interface {
	FindByID(primitive.ObjectID) (*models.User, error)
	InsertOne(*models.User) (primitive.ObjectID, error)
	FindOne(filter interface{}) (*models.User, error)
}

type userRepoImpl struct {
}

func User() UserRepo {
	return &userRepoImpl{}
}

func (*userRepoImpl) FindByID(_id primitive.ObjectID) (*models.User, error) {
	var ret models.User

	filter := bson.M{"_id": _id}
	if err := db.MongoDB().Collection((&models.User{}).CollectionName()).FindOne(context.TODO(), filter).Decode(&ret); err != nil {
		return &ret, err
	}

	return &ret, nil
}

func (*userRepoImpl) InsertOne(user *models.User) (primitive.ObjectID, error) {
	var (
		err error
	)
	user.ID = primitive.NewObjectID()
	user.CreatedAt = time.Now()
	user.UpdatedAt = time.Now()
	user.Pwd, err = utils.BcryptHash(user.Pwd)
	user.Role = "user"
	if err != nil {
		return primitive.NilObjectID, err
	}

	res, err := db.MongoDB().Collection(user.CollectionName()).InsertOne(context.TODO(), user)
	if err != nil {
		return primitive.NilObjectID, err
	}

	return res.InsertedID.(primitive.ObjectID), nil
}

func (*userRepoImpl) FindOne(filter interface{}) (*models.User, error) {
	var ret models.User
	if err := db.MongoDB().Collection((&models.User{}).CollectionName()).FindOne(context.TODO(), filter).Decode(&ret); err != nil {
		return nil, err
	}

	return &ret, nil
}
