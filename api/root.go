package api

import (
	"log"
	"net/http"
	"vieturbandict/api/middlewares"
	"vieturbandict/api/routes"
	"vieturbandict/db"
	"vieturbandict/rest"

	"github.com/gin-gonic/gin"
)

type App struct {
	router *gin.Engine
}

func (a *App) initialize() {
	db.MongoDBConnect()
	db.RedisConnect()

	a.router = gin.New()
	a.initRoutes()
}

func (a *App) initRoutes() *gin.Engine {
	router := a.router
	router.Use(middlewares.ExceptionInterceptor)
	router.Use(middlewares.CORS)
	router.Use(middlewares.AddContext)

	routes := routes.Collect()
	a.preInitRoutes(&routes)

	return router
}

func (a *App) preInitRoutes(routes *rest.Routes) {
	for _, route := range *routes {
		var handlersChain gin.HandlersChain

		if route.HandlersChain != nil {
			handlersChain = route.HandlersChain
		} else {
			handlersChain = gin.HandlersChain{route.HandlerFunc}
		}

		if len(route.Permit) > 0 {
			handlersChain = append(gin.HandlersChain{middlewares.Permit(route.Permit...)}, handlersChain...)
		}

		if !route.Public {
			handlersChain = append(gin.HandlersChain{middlewares.Auth}, handlersChain...)
		}

		route.HandlersChain = handlersChain
		a.router.Handle(route.Method, route.Pattern, route.HandlersChain...)
	}
}

func (a *App) Run(addr string) {
	a.initialize()

	defer func() {
		log.Fatal(http.ListenAndServe(addr, a.router))
	}()

	log.Printf("vieturbandict APIs is running at %v", addr)
}
