package dtos

type LoginBody struct {
	UserName string `json:"username"`
	Pwd      string `json:"password"`
}
