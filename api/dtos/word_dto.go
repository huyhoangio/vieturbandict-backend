package dtos

type NewWordBody struct {
	Word                           string   `json:"word"`
	Explanation_in_Vietnamese      string   `json:"explanation_in_vietnamese"`
	Literal_translation_to_English string   `json:"literal_translation_to_english"`
	Explanation_in_English         string   `json:"explanation_in_english"`
	Equivalence_in_English         []string `json:"equivalence_in_english"`
	Upvote                         int      `json:"upvote"`
	Downvote                       int      `json:"downvote"`
}
