package utils

import "golang.org/x/crypto/bcrypt"

func BcryptHash(pwd string) (string, error) {
	ret, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.DefaultCost)
	return string(ret), err
}
