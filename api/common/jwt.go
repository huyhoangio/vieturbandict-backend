package common

import "github.com/dgrijalva/jwt-go"

type JWTClaims struct {
	UserID string `json:"user_id"`
	IP     string `json:"ip,omitempty"`
	RID    string `json:"rid"`
	jwt.StandardClaims
}
