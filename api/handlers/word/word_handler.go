package word_handlers

import (
	"net/http"
	"strconv"
	"strings"
	"vieturbandict/api/constants"
	"vieturbandict/api/models"
	"vieturbandict/api/services"
	"vieturbandict/rest"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetRecentWords(c *gin.Context) {
	_page, _ := strconv.Atoi(c.Query("page"))
	page := int64(_page)
	_size, _ := strconv.Atoi(c.Query("pageSize"))
	size := int64(_size)
	all := c.Query("all")
	if page == 0 {
		page = 1
	}
	if size == 0 {
		size = 10
	}
	_status := strings.Split(c.Query("status"), ",")
	status := []string{}
	for _, e := range _status {
		if e != "" {
			status = append(status, e)
		}
	}
	user, exists := c.Get("user")
	userId := primitive.NilObjectID
	role := constants.USER
	if exists {
		userId = user.(*models.User).ID
		role = user.(*models.User).Role
	}
	if all == "1" {
		userId = primitive.NilObjectID
	}

	words, _ := services.Word().GetRecents(page, size, role, status, userId)
	total, err := services.Word().CountDocuments(page, size, role, status, userId)

	if err != nil {
		c.JSON(http.StatusInternalServerError, rest.Response{
			Status: 1,
			Code:   http.StatusOK,
			Data:   nil,
			Page:   page,
			Size:   size,
			Total:  0,
		})
		return
	}

	c.JSON(http.StatusOK, rest.Response{
		Status: 1,
		Code:   http.StatusOK,
		Data:   words,
		Page:   page,
		Size:   size,
		Total:  total,
	})
}

func AddNew(c *gin.Context) {
	var word models.Word
	c.ShouldBindJSON(&word)
	user, exists := c.Get("user")
	createdBy := primitive.NilObjectID
	if exists {
		createdBy = user.(*models.User).ID
	}

	insID, err := services.Word().AddNew(&word, createdBy)
	if err != nil {
		panic(rest.Error{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	insWord, err := services.Word().FindByID(insID)
	if err != nil {
		panic(rest.Error{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	if insWord == nil {
		panic(rest.Error{
			Code:    http.StatusNotFound,
			Message: "word NOT found",
		})
	}
	c.JSON(http.StatusCreated, rest.Response{
		Status: 1,
		Code:   http.StatusCreated,
		Data: gin.H{
			"word": insWord,
		},
	})
}

func FindByID(c *gin.Context) {
	wordID := c.Param("id")
	wordOid, _ := primitive.ObjectIDFromHex(wordID)
	word, err := services.Word().FindByID(wordOid)
	if err != nil {
		panic(rest.Error{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	c.JSON(http.StatusOK, rest.Response{
		Status: 1,
		Code:   http.StatusOK,
		Data: gin.H{
			"word": word,
		},
	})
}

func Approve(c *gin.Context) {
	wordID := c.Param("id")
	wordOid, _ := primitive.ObjectIDFromHex(wordID)
	_, err := services.Word().Consider(wordOid, "approved")
	if err != nil {
		panic(rest.Error{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	c.JSON(http.StatusOK, rest.Response{
		Status: 1,
		Code:   http.StatusOK,
		Data: gin.H{
			"message": "Success",
		},
	})
}

func Disapprove(c *gin.Context) {
	wordID := c.Param("id")
	wordOid, _ := primitive.ObjectIDFromHex(wordID)
	_, err := services.Word().Consider(wordOid, "disapproved")
	if err != nil {
		panic(rest.Error{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	c.JSON(http.StatusOK, rest.Response{
		Status: 1,
		Code:   http.StatusOK,
		Data: gin.H{
			"message": "Success",
		},
	})
}

func Delete(c *gin.Context) {
	wordID := c.Param("id")
	wordOid, _ := primitive.ObjectIDFromHex(wordID)
	err := services.Word().DeleteOne(wordOid)
	if err != nil {
		panic(rest.Error{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	c.JSON(http.StatusOK, rest.Response{
		Status: 1,
		Code:   http.StatusOK,
		Data: gin.H{
			"message": "Success",
		},
	})
}
