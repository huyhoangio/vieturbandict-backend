package auth_handlers

import (
	"net/http"
	"vieturbandict/api/dtos"
	"vieturbandict/api/models"
	"vieturbandict/api/services"
	"vieturbandict/rest"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func Register(c *gin.Context) {
	var (
		body         dtos.CreateUserBody
		err          error
		userObjectID primitive.ObjectID
		createdUser  *models.User
	)
	c.BindJSON(&body)
	user := models.User{FirstName: body.FirstName, LastName: body.LastName, UserName: body.UserName, Pwd: body.Pwd}
	userObjectID, err = services.User().AddNew(&user)
	if err != nil {
		panic(rest.Error{
			Code:    http.StatusInternalServerError,
			Message: "Can NOT create user",
		})
	}
	createdUser, err = services.User().FindByID(userObjectID)
	if err != nil {
		panic(rest.Error{
			Code:    http.StatusInternalServerError,
			Message: "Errors in creating user",
		})
	}
	c.JSON(http.StatusOK, rest.Response{
		Status: 1,
		Code:   http.StatusOK,
		Data:   createdUser,
	})
}
