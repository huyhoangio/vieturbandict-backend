package auth_handlers

import (
	"net/http"
	"vieturbandict/api/dtos"
	"vieturbandict/api/services"
	"vieturbandict/rest"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func Login(c *gin.Context) {
	var (
		user dtos.LoginBody
	)
	if err := c.ShouldBindJSON(&user); err != nil {
		panic(rest.Error{
			Code:    http.StatusUnprocessableEntity,
			Message: "Please provide valid json",
		})
	}
	authUser, err := services.User().FindByUserName(user.UserName)
	if authUser == nil || err != nil {
		panic(rest.Error{
			Code:    http.StatusUnauthorized,
			Message: "Username or password was wrong",
		})
	}

	if err := bcrypt.CompareHashAndPassword([]byte(authUser.Pwd), []byte(user.Pwd)); err != nil {
		panic(rest.Error{
			Code:    http.StatusUnauthorized,
			Message: "Username or password was wrong",
		})
	}

	ip := services.Auth().GetIP(c.Request)
	token, _ := services.Auth().GenerateAccessToken(authUser.ID.Hex())
	refreshToken, _ := services.Auth().GenerateRefreshToken(authUser.ID.Hex(), ip)

	c.JSON(http.StatusOK, rest.Response{
		Status: 1,
		Code:   http.StatusOK,
		Data: gin.H{
			"user":          authUser,
			"token":         token,
			"refresh_token": refreshToken,
		},
	})
}
