package auth_handlers

import (
	"net/http"
	"os"
	"vieturbandict/api/services"
	"vieturbandict/rest"

	"github.com/gin-gonic/gin"
)

func GrantNewAccessToken(c *gin.Context) {
	rfCookie, err := c.Request.Cookie("x_refresh_token")
	if err != nil {
		panic(err)
	}
	refreshToken := rfCookie.Value
	claims, _ := services.Auth().ParseToken(refreshToken, os.Getenv("JWT_REFRESH_SECRET"))
	token, _ := services.Auth().GenerateAccessToken(claims.UserID)
	c.JSON(http.StatusCreated, rest.Response{
		Status: 1,
		Code:   http.StatusCreated,
		Data: gin.H{
			"token": token,
		},
	})
}
