package middlewares

import (
	"net/http"
	"os"
	"reflect"
	"strings"
	"vieturbandict/rest"

	"github.com/gin-gonic/gin"
)

func ExceptionInterceptor(c *gin.Context) {
	const DEFAULT_ERROR_MESSAGE = "Internal server error"
	defer func() {
		if err := recover(); err != nil {
			var restErr rest.Error

			if os.Getenv("ENV") == "production" {
				restErr = rest.Error{
					Code:    http.StatusInternalServerError,
					Message: DEFAULT_ERROR_MESSAGE,
				}
			}

			if strings.ToLower(os.Getenv("MESSAGE_ON")) != "true" {
				restErr.Message = DEFAULT_ERROR_MESSAGE
			}

			ok := reflect.TypeOf(err) == reflect.TypeOf(rest.Error{})
			if ok {
				restErr = err.(rest.Error)
			} else {
				panic(err)
			}

			c.AbortWithStatusJSON(restErr.Code, rest.Response{
				Status: 0,
				Error:  &restErr,
			})
		}
	}()

	c.Next()
}
