package middlewares

import (
	"net/http"
	"vieturbandict/api/models"
	"vieturbandict/rest"

	"github.com/gin-gonic/gin"
)

func Permit(roles ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
		_user, exists := c.Get("user")
		if !exists {
			panic(rest.Error{
				Code:    http.StatusUnauthorized,
				Message: "Unauthorized",
			})
		}

		user := _user.(*models.User)

		if user.Role == "admin" {
			c.Next()
			return
		}
		for _, role := range roles {
			if role == user.Role {
				c.Next()
				return
			}
		}

		panic(rest.Error{
			Code:    http.StatusUnauthorized,
			Message: "Unauthorized",
		})
	}
}
