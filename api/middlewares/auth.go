package middlewares

import (
	"net/http"
	"vieturbandict/rest"

	"github.com/gin-gonic/gin"
)

func Auth(c *gin.Context) {
	_, exists := c.Get("user")
	if !exists {
		panic(rest.Error{
			Code:    http.StatusUnauthorized,
			Message: "Protected! Need to authenticate",
		})
	}

	c.Next()
}
