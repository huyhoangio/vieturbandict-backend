package middlewares

import (
	"os"
	"vieturbandict/api/services"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func AddContext(c *gin.Context) {
	req := c.Request
	accessToken := services.Auth().ExtractToken(req)

	if accessToken != "" {
		claims, _ := services.Auth().ParseToken(accessToken, os.Getenv("JWT_ACCESS_SECRET"))

		userOid, _ := primitive.ObjectIDFromHex(claims.UserID)
		user, _ := services.User().FindByID(userOid)
		c.Set("user", user)
	}

	c.Next()
}
