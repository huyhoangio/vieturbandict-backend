package routes

import (
	auth_handlers "vieturbandict/api/handlers/auth"
	ping_handlers "vieturbandict/api/handlers/ping"
	word_handlers "vieturbandict/api/handlers/word"
	"vieturbandict/rest"
)

func Collect() rest.Routes {
	routes := rest.Routes{
		rest.Route{
			Name:        "Ping",
			Method:      "GET",
			Pattern:     "/healthcheck",
			HandlerFunc: ping_handlers.Ping,
			Public:      true,
		},

		rest.Route{
			Name:        "Register",
			Method:      "POST",
			Pattern:     "/register",
			HandlerFunc: auth_handlers.Register,
			Public:      true,
		},

		rest.Route{
			Name:        "Login",
			Method:      "POST",
			Pattern:     "/auth/login",
			HandlerFunc: auth_handlers.Login,
			Public:      true,
		},

		rest.Route{
			Name:        "Grant access token",
			Method:      "GET",
			Pattern:     "/auth/new_access_token",
			HandlerFunc: auth_handlers.GrantNewAccessToken,
			Public:      true,
		},

		rest.Route{
			Name:        "List all words",
			Method:      "GET",
			Pattern:     "/words",
			HandlerFunc: word_handlers.GetRecentWords,
			Public:      true,
		},

		rest.Route{
			Name:        "Create new word",
			Method:      "POST",
			Pattern:     "/words",
			HandlerFunc: word_handlers.AddNew,
			Public:      true,
		},

		rest.Route{
			Name:        "Get a word",
			Method:      "GET",
			Pattern:     "/words/:id",
			HandlerFunc: word_handlers.FindByID,
			Public:      true,
		},

		rest.Route{
			Name:        "Approve new word",
			Method:      "PUT",
			Pattern:     "/words/:id/approve",
			HandlerFunc: word_handlers.Approve,
			Public:      false,
			Permit:      []string{"mod", "admin"},
		},

		rest.Route{
			Name:        "Disapprove a word",
			Method:      "PUT",
			Pattern:     "/words/:id/disapprove",
			HandlerFunc: word_handlers.Disapprove,
			Public:      false,
			Permit:      []string{"admin"},
		},

		rest.Route{
			Name:        "Delete a word",
			Method:      "DELETE",
			Pattern:     "/words/:id/delete",
			HandlerFunc: word_handlers.Delete,
			Public:      false,
			Permit:      []string{"admin"},
		},
	}

	return routes
}
