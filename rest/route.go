package rest

import (
	"github.com/gin-gonic/gin"
)

type Route struct {
	Name          string
	Method        string
	Pattern       string
	HandlerFunc   gin.HandlerFunc
	HandlersChain gin.HandlersChain
	Public        bool
	Permit        []string
}

type Routes []Route
