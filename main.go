package main

import (
	"log"
	"os"

	"vieturbandict/api"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()

	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}

	app := api.App{}
	app.Run(":" + port)
}
